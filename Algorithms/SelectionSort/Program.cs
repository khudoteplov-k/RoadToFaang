﻿var generator = new Random();
var array = new int[100];

for (int i = 0; i < array.Length; i++)
{
    array[i] = generator.Next(0, 100);
    Console.Write(array[i] + " ");
}

Console.WriteLine("\n\n");

int smallest;
for (int i = 0; i < array.Length; i++)
{
    smallest = i;
    for (int j = i + 1; j < array.Length; j++)
    {
        if (array[j] < array[smallest])
        {
            smallest = j;
        }
    }

    (array[smallest], array[i]) = (array[i], array[smallest]);
}

foreach (var num in array)
{
    Console.Write(num + " ");
}