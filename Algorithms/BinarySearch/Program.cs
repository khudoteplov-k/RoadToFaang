﻿var rand = new Random();
int low = 0, high = 1000;

var num = rand.Next(low, high);

for (int i = (high - low) / 2;; i = (high - low) / 2 + low)
{
    Console.Write($"{i} - ");
    
    if (i == num)
    {
        Console.Write("You win.");
        break;
    }
    else if (i < num)
    {
        low = i;
        Console.WriteLine("Lower.");
    }
    else
    {
        high = i;
        Console.WriteLine("Higher.");
    }
}