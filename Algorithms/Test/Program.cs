﻿Console.WriteLine(Func4_1(new int[] { 2, 3, 1, 5 }, 0));
Console.WriteLine(Func4_2(new int[] { 2, 3, 5, 12 }, 0));
Console.WriteLine(Func4_3(new int[] { 1, 2, 3, 4, 5 }, 0));

int Func4_1(int[] nums, int from)
{
    if (nums.Length == from)
    {
        return 0;
    }

    return nums[from] + Func4_1(nums, from + 1);
}
int Func4_2(int[] nums, int from)
{
    if (nums.Length == from)
    {
        return 0;
    }

    return 1 + Func4_2(nums, from + 1);
}
int Func4_3(int[] nums, int from)
{
    if (nums.Length == from)
    {
        return 0;
    }
    
    return Math.Max(nums[from], Func4_3(nums, from + 1));
}