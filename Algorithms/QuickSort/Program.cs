﻿var generator = new Random();
var arr = new int[10000];

for (int i = 0; i < arr.Length; i++)
{
    arr[i] = generator.Next(0, 100);
    Console.Write(arr[i] + " ");
}

Console.WriteLine("\n\n");
QuickSort(arr, 0, arr.Length - 1);

for (int i = 0; i < arr.Length; i++)
{
    Console.Write(arr[i] + " ");
}

int[] QuickSort(int[] array, int left, int right)
{
    var pivot = array[left];
    int i = left, j = right;
    while (i <= j)
    {
        while (array[i] < pivot)
        {
            i++;
        }
        while (array[j] > pivot)
        {
            j--;
        }

        if (i <= j)
        {
            (array[j], array[i]) = (array[i], array[j]);
            i++;
            j--;
        }
    }

    if (left < j)
    {
        QuickSort(array, left, j);
    }
    if (right > i)
    {
        QuickSort(array, i, right);
    }

    return array;
}